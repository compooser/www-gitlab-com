---
layout: handbook-page-toc
title: "Gainsight Maturity Scoring"
description: "An overview of Maturity Scoring, how it is calculated, and how TAMs can use the information with customers in their conversations."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

*For an overview of Gainsight, and information about how to login, please refer to the [Gainsight Overview Page](/handbook/sales/gainsight/).*

For an overview of how TAMs use Gainsight, please refer to the [Gainsight TAM Overview Page](/handbook/customer-success/tam/gainsight)

## Maturity Scoring

Use case maturity scoring will assist TAMs in understanding a customer's adoption state based on a specific list of metrics. 
By looking at the maturity scores, the TAM will gain an understanding of the customer's current state in the adoption journey.


### CI Maturity Scoring

The following primary and ancillary metrics are used to determine a customer's CI Maturity Score. 

<br>
![CI Maturity Scoring](/handbook/customer-success/product-usage-data/maturity-scoring/ci-maturity-scoring.png)
<br>
[Adoption Guide Reference Link](/handbook/marketing/strategic-marketing/usecase-gtm/ci/#adoption-guide)

In this (internal) video a fellow TAM walks you through:
- how the CI maturity score is calculated 
- template slides to showcase a customer's maturity
- key conversation drivers such as:
  - how the customer is currently progressing on their path to CI maturity
  - how the customer compares with other similar customer's in the industry 
  - key areas of focus on the adoption journey 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/zurUFQDSWt8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>  
<!-- blank line -->

#### Resources

- [Use Case Maturity Scoring - Defined Metrics](https://docs.google.com/spreadsheets/d/1lawzRG6yOCTYl2CK9zAaBCUFU6UiSZz65eMiQxRGhLI/edit#gid=1882408673) (internal only)
- [Template Deck for Customer Conversations](https://docs.google.com/presentation/d/1Zn5gyUrBRgA1fyprVuoA24FKiH_3fpT5KuL5vK6GcuE/edit#slide=id.g110af81e0a3_0_215) (internal only)
    - In order to generate the slides shown in the video above - this [Google sheet](https://docs.google.com/spreadsheets/d/1wPrQRS9XGJek4oWcZPe9QeaFne9scbJVZYuvEioE2GI/edit#gid=1737266116) can help. 
        - Instructions on using the sheet can be found in this [internal video](https://youtu.be/oWuX_jtLnLI)

#### Additional TAM Enablement 

- CI Maturity Scoring Walk-thru 
    - [Session 1](https://youtu.be/E4IMgFWGkNM) (internal only)
    - [Session 2](https://chorus.ai/meeting/E4F00AFC0C4A4036A7AC370653A50112?) (internal only)

