---
layout: markdown_page
title: "Next Prioritization"
description: "Transition R&D teams into a steady priorization state driven by backlog data, SLO guidelines & healthy prioritization ratios"
canonical_path: "/company/team/structure/working-groups/next-prioritization/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value      |
|-----------------|------------|
| Date Created    | 2022-04-13 |
| Target End Date | 2022-06-30 |
| Slack           | [#wg_next-prioritization](https://gitlab.slack.com/archives/C03AWM7780G) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1wog8bR7jg6SECefx9BGqIa07sFm_sXJPvelVAganYmc/edit#heading=h.pmtw3ocv2aty)  |
| Task Board      | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/4199535?label_name[]=wg_maint-first) |
| Epic            | [Link](https://gitlab.com/groups/gitlab-com/-/epics/1799) |

## Business Goal

Transition R&D teams immediately to this new model driven by backlog types, automated SLO guidelines & healthy prioritization ratios. This should not wait until complete burn-down of their Reliability & Security debt backlog (estimate all teams by the end of May). But we should expect maintenance to be high in this new model, until we reach that steady state.

* Teams backlog slates containing view of all backlog types.
  * Security
  * Reliability
  * UX / SUS
  * Bugs
* Teams merged work type slate (historical & real time)
* Stakeholders slate, view of progress and ratio attainment
* Automation guardrails to enforce hygiene & SLO
* Handbook team dashboards

[Work type improvements & initial dashboards have been completed prior to the working group.](https://gitlab.com/groups/gitlab-com/-/epics/1799#pre-work-by-april-15th) 


### Exit Criteria (0% completed)

1. Ship new name of steady state priorization
1. Company-wide comms by 2022-04-30
1. Refine stakeholder dashboard views, bug & maintenance backlog types
1. Reduce undefined MR types (less than 5%)
1. Set target x% of product group teams to Steady state
1. Setup monthly MR type reviews between engineering managers
1. Handbook work & embed team dashboards in team pages
1. Bot automation work
1. Mature error budgets by 2022-05-30
1. Set goal for Development department-wide Steady state (tentative 2022-06-15)
1. Other handbook cleanup


### Roles and Responsibilities

| Working Group Role    | Person                                               | Title                                                      |
|-----------------------|------------------------------------------------------|------------------------------------------------------------|
| Executive Sponsor     | Eric Johnson                                         | CTO                                                        |
| Facilitator           | Wayne Haber                                          | Director of Engineering                                    |
| Functional Lead       | Christopher Lefelhocz                                | VP of Development                                          | 
| Functional Lead       | Valerie Karnes                                  | Director of Product Design |
| Functional Lead       | Mek Stittri                                          | VP of Quality |
| Functional Lead       | David DeSanto                                        | VP of Product Management |
| Functional Lead       | Justin Farris                                        | Senior Director of Product Management |
| Functional Lead       | Farnoosh Seifoddini                                  | Head of Product Operations |
| Member                | Lily Mai                                             | Staff Engineering Analyst |
| Member                | Tanya Pazitny                                        | Director of Quality Engineering |
| Member                | Kyle Wiebers                                         | Engineering Manager, Engineering Productivity |
| Member                | John Hope                                            | Engineering Manager, Plan:Product Planning & Certify |
| Member                | Matt Wilson                                          | Senior Product Manager, Secure |
| Member                | Neil McCorrison                                      | Frontend Engineering Manager, Secure |
      
