---
layout: markdown_page
title: "GitLab Sweepstakes"
description: "View a full listing of current and previous GitLab Sweepstakes giveways. Find more information here!"
canonical_path: "/community/sweepstakes/"
---
<!-- Housekeeping: Please deprecate any Sweepstakes drawn more than 60 days ago. -->
## Current and previous giveaways

### Current
- [GitLab IT Revolution iPad Sweepstakes](https://about.gitlab.com/community/sweepstakes/gitlab-it-revolution-ipad-sweepstakes/)

### Past
- [GitLab Survey weekly digest 2022](/community/sweepstakes/2022-survey-weekly-digest/survey-weekly-digest.index.html)

